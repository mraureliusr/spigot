#!/usr/bin/env python3

import sys


class Spigot:
    def __init__(self, rlength):
        self.length = rlength
        self.spigotList = [2 for x in range(0, self.length)]
        self.carry = 0
        self.preDigits = []

    def pump(self):
        # Multiply all values by 10
        i = self.length - 1
        while i >= 0: # notice this *is* handling zero
            try:
                self.spigotList[i] = self.spigotList[i] * 10
                i = i - 1
            except IndexError:
                print(f"i is {i}, length is {self.length}")
                sys.exit()
        # do the crazy spigot algorithm
        self.carry = 0
        i = self.length - 1
        while i > 0: # notice this is *not* handling i=0
            self.spigotList[i] = self.spigotList[i] + self.carry # add any carry from previous column
            self.carry = (self.spigotList[i] // (i * 2 + 1)) * i # reduce by column's denom, then multiply by column number, which is numerator
            self.spigotList[i] = self.spigotList[i] % (i * 2 + 1) # reduce by colum's denom (2x+1)
            i = i - 1

        # special case of i = 0
        self.spigotList[0] = self.spigotList[0] + self.carry
        tempPreDigit = self.spigotList[0] // 10 # get q
        self.spigotList[0] = self.spigotList[0] - (tempPreDigit * 10) # and remove it from the current value
        # handle the different cases depending on the predigit
        if (tempPreDigit >= 0) and (tempPreDigit <= 8): # handle digits 0 through 8
            while self.preDigits:
                print(f"{self.preDigits.pop()}", end="")
            self.preDigits.insert(0, tempPreDigit)
        elif tempPreDigit == 9:
            self.preDigits.insert(0, tempPreDigit)
        elif tempPreDigit == 10:
            tempPreDigit = 0
            while self.preDigits:
                i = (self.preDigits.pop() + 1) % 10
                print(f"{i}", end="")
            self.preDigits.insert(0, tempPreDigit)
        else:
            print("We should never get here!") # in case we get a digit bigger than 10, which shouldn't happen
        sys.stdout.flush() # gives that spigot look

    def printState(self):
        for i in self.spigotList:
            print(i, end=",")


def main():
    try:
        digits = int(input("Enter how many digits you want: "))
    except ValueError:
        print("Blank entry defaults to 100 digits.")
        digits = 100
    except Exception:
        print("Invalid entry: quitting...")
        sys.exit(1)
    testSpigot = Spigot(int((digits * 10) / 3))
    for i in range(0, digits):
        testSpigot.pump()

if __name__ == "__main__":
    main()
