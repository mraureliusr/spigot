# PySpigot

### Pi Day 2023
In celebration of Pi Day 2023 (3/14) I decided to implement one of the many pi-creating algorithms out there. The spigot algorithm in particular always intrigued me, as it doesn't use any floating point math, and therefore can run on pretty much any platform. With the experience of writing this for Python, I might try and write a version for the VIC-20 if I have the time.

The Spigot algorithm also works for calculating digits of e, which is actually my favourite number (pi is cool and all, but e makes the world go round). So at some point I might add that algorithm into this script and let the user pick between them.

Of course this is totally pointless as if you want lots of digits of pi, you can get literally trillions using stuff like y-cruncher. But, I learned how this particular algorithm works (well, sort of -- I understand what it *does* but I don't fully understand how it ends up with pi as the output).

Feel free to poke around and make changes or use it for whatever you want -- it's licensed under the Mozilla Public Licence v2, as you can read in LICENSE.